#!/bin/sh
DESC="initial branch restoring tests with worktrees"
. "$(dirname "$0")/lib.sh"

test_repository()
{
    git init -q master
    (
        # initial branch
        cd master
        touch file_master
        git add file_master
        commit

        # worktree
        git worktree add ../worktree -b worktree -q

        # support branch
        checkout -b intermediate

        # updates branch
        checkout -b updates
        touch file_updates
        git add file_updates
        commit

        # reset initial branch
        checkout master
    )

    (
        # generate an assembly file with the following structure:
        #   worktree <- intermediate <- updates*
        # the intermediate branch is used to force a branch switch in the "master" tree
        # for the temporary work that needs to be correctly restored
        cd master
        cat <<EOF > .git/assembly
merge intermediate updates
merge worktree intermediate
EOF
    )
}

verb "test regular operations"
(
    verb ".. create test repository"
    mkdir test_regular
    cd test_regular
    test_repository

    verb ".. run the assembly process"
    cd master
    capture gas -av

    verb ".. ensure the master tree is unchanged"
    test $(current_branch) = master
    not test -f file_updates

    verb ".. ensure the worktree has been updated"
    cd ../worktree
    test $(current_branch) = worktree
    test -f file_updates
)

verb "test interrupt/restore from master tree"
(
    verb ".. create test repository"
    mkdir test_interrupted
    cd test_interrupted
    test_repository

    verb ".. create an explicit conflit"
    not test -f worktree/file_updates
    touch worktree/file_updates

    verb ".. ensure the operation has interrupted"
    cd master
    capture not gas -av
    assert_out_regex "error while merging intermediate into worktree(\.\./worktree)"

    verb ".. ensure the master tree has been changed"
    test $(current_branch) = intermediate

    verb ".. remove the conflict and continue from the master tree"
    cd ../worktree
    test $(current_branch) = worktree
    rm file_updates
    cd ../master
    quiet gas -av

    verb ".. ensure the master tree has been restored"
    test $(current_branch) = master

    verb ".. ensure the worktree has been updated"
    cd ../worktree
    test $(current_branch) = worktree
    test -f file_updates
)

verb "test interrupt/restore from worktree"
(
    verb ".. create test repository"
    mkdir test_wt_restore
    cd test_wt_restore
    test_repository

    verb ".. create an explicit conflit"
    not test -f worktree/file_updates
    touch worktree/file_updates

    verb ".. ensure the operation has interrupted"
    cd master
    capture not gas -av
    assert_out_regex "error while merging intermediate into worktree(\.\./worktree)"

    verb ".. ensure the master tree has been changed"
    test $(current_branch) = intermediate

    verb ".. remove the conflict and continue from the worktree"
    cd ../worktree
    test $(current_branch) = worktree
    rm file_updates
    quiet gas -av

    verb ".. ensure the worktree has been updated"
    test $(current_branch) = worktree
    test -f file_updates

    verb ".. ensure the master tree has been restored"
    cd ../master
    test $(current_branch) = master
)
