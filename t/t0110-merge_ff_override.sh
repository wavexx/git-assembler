#!/bin/sh
DESC="merge tests with conflicting assembler.mergeff settings"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with two branches"
git init -q
touch file_master
git add file_master
commit
checkout -b test
touch file_test
git add file_test
commit
checkout master
not test -f file_test

verb "add one extra commit to prevent fast-forward merges"
touch file2_master
git add file2_master
commit

verb "setup merge.ff=only for this repository"
git config merge.ff only

verb "initialize assembly file"
cat <<EOF > .gitassembly
merge master test
EOF

verb "ensure non-ff merge fails by default with merge.ff=only"
quiet not gas -av

verb "ensure merge.ff can be overridden through assembler.mergeff"
git config assembler.mergeff false
capture gas -av
assert_out_regex "merging test into master using --no-ff"
test -f file_test
