#!/bin/sh
DESC="branch name encoding tests"
. "$(dirname "$0")/lib.sh"

if [ `uname` = "Darwin" ]; then
    skip
fi

verb "initialize a new repository"
git init -q
touch file
git add file
commit

verb "enforce an ascii locale"
export LC_ALL=C

verb "create assembly file with invalid locale characters"
printf 'base br\344nch master\n' > .gitassembly

verb "ensure the branch name can be read back without errors"
capture gas -av --create --dry-run
assert_out_regex 'creating branch [^ ]* from master'

verb "ensure the branch name can be created without errors"
capture gas -ac

verb "ensure the created branch name is byte-exact"
printf 'br\344nch\n' > expected
git branch -l --format='%(refname:short)' 'b*' > output
cmp output expected
