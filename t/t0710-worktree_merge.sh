#!/bin/sh
DESC="basic merge tests on worktrees"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo and worktree"
git init -q master
(
    cd master
    touch file_master
    git add file_master
    commit
    git worktree add ../worktree -b test -q
    touch file_new
    git add file_new
    commit
)
(
    cd worktree
    touch file_test
    git add file_test
    commit
)

verb "initialize assembly file"
cd master
cat <<EOF > .gitassembly
merge test master
EOF

verb "ensure merge is run in the worktree"
capture gas -av
assert_out_regex 'merging master into test(\.\./worktree)'
test -f file_master
not test -f file_test
test -f ../worktree/file_master
