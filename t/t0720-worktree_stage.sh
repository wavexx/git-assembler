#!/bin/sh
DESC="basic stage tests on worktrees"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo and worktree"
git init -q master
(
    cd master
    touch file_master
    git add file_master
    commit
    git worktree add ../worktree -b test -q
    touch file_new
    git add file_new
    commit
)

verb "initialize assembly file"
cd master
cat <<EOF > .gitassembly
stage test master
EOF

verb "ensure stage is run in the worktree"
capture gas -av
assert_out_regex 'creating branch test(\.\./worktree) from master'
test -f ../worktree/file_new
