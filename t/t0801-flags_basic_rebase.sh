#!/bin/sh
DESC="check that rebase flags are passed onto git"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with two branches"
git init -q
touch file_master
git add file_master
commit
checkout -b test

_BASE_HEAD="$(git rev-parse HEAD)"

_reset() {
    checkout master
    git reset --hard -q "$_BASE_HEAD"
    checkout test
    git reset --hard -q master
    touch file_test
    git add file_test
    commit
    checkout master
    not test -f file_test
}

verb "testing rebase without flags"
(
    _reset
    cat <<EOF > .gitassembly
    rebase master test
EOF
    capture gas -av
    assert_out_regex "rebasing master onto test"
)

verb "testing rebase with flags"
(
    _reset
    cat <<EOF > .gitassembly
    rebase master test -s ours
EOF
    capture gas -av
    assert_out_regex "rebasing master onto test using -s ours"
)
