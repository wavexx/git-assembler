#!/bin/sh
DESC="expansion tests for targets"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with several branches"
git init -q
touch file_master
git add file_master
commit

n=0
for branch in test_a test_b
do
  verb "creating branch $branch"
  checkout -b "$branch"
  touch file_$n
  git add file_$n
  commit
  n=$(($n + 1))
done

verb "test cli target expansion"
cat <<EOF > .gitassembly
merge test_a test_b
EOF
capture gas -a --dry-run 'test_*'
assert_out_regex "merging test_b into test_a"

verb "test cli target expansion failure"
cat <<EOF > .gitassembly
merge test_a test_b
EOF
capture not gas -a --dry-run 'test_d'
assert_out_regex "unknown target branch test_d"
capture not gas -a --dry-run 'test_d*'
assert_out_regex "pattern test_d\* in argument does not match any node"

verb "test settings target expansion"
cat <<EOF > .gitassembly
merge test_a test_b
target test_*
EOF
capture gas -a --dry-run
assert_out_regex "merging test_b into test_a"

verb "test settings unknown target"
cat <<EOF > .gitassembly
merge test_a test_b
target test_d
EOF
capture not gas -a --dry-run
assert_out_regex "unknown target branch test_d"

verb "test settings target expansion failure"
cat <<EOF > .gitassembly
merge test_a test_b
target test_d*
EOF
capture not gas -a --dry-run
assert_out_regex "pattern test_d\* in argument does not match any node"

