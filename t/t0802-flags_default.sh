#!/bin/sh
DESC="check that default flags compose with merge/rebase"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with two branches"
git init -q
touch file_master
git add file_master
commit
checkout -b test

_BASE_HEAD="$(git rev-parse HEAD)"

_reset() {
    checkout master
    git reset --hard -q "$_BASE_HEAD"
    checkout test
    git reset --hard -q master
    touch file_test
    git add file_test
    commit
    checkout master
    not test -f file_test
}

verb "testing merge without flags"
(
    _reset
    cat <<EOF > .gitassembly
    merge master test
EOF
    capture gas -av
    assert_out_regex "merging test into master"
)

verb "testing merge with default flags"
(
    _reset
    cat <<EOF > .gitassembly
    flags merge --ff
    merge master test
EOF
    capture gas -av
    assert_out_regex "merging test into master using --ff"
)

verb "testing default flags reset"
(
    _reset
    cat <<EOF > .gitassembly
    flags merge --ff
    flags merge
    merge master test
EOF
    capture gas -av
    assert_out_regex "merging test into master"
)

verb "testing default flags with rule flags"
(
    _reset
    cat <<EOF > .gitassembly
    flags merge --ff
    merge master test -s ours
EOF
    capture gas -av
    assert_out_regex "merging test into master using --ff -s ours"
)
