#!/bin/sh
DESC="check for assembly state consistency checks"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with a conflict"
git init -q
touch file
git add file
commit
checkout -b test
echo a > file
git add file
commit
checkout master
echo b > file
git add file
commit
checkout -b temp master
test $(current_branch) = temp

verb "perform a conflicting merge"
cat <<EOF > .gitassembly
merge master test
EOF
capture not gas -a
assert_out_regex "error while merging test into master"
test $(current_branch) = master

verb "simulate inconsistent state"
test -f .git/as-cache/state
rm .git/as-cache/state
capture not gas -a
assert_out_regex "inconsistent state: .*"

verb "simulate a different initial version"
test -f .git/as-cache/version
echo "1.3" > .git/as-cache/version
capture not gas -a
assert_out_regex "incompatible state: .*"
