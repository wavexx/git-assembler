#!/bin/sh
DESC="merge duplicates"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with two branches"
git init -q
touch file_master
git add file_master
commit
checkout -b branch1
touch file_branch1
git add file_branch1
commit
checkout -b branch2
touch file_branch2
git add file_branch2
commit
checkout master
test -f file_master
not test -f file_branch1
not test -f file_branch2

verb "initialize assembly file with a merge into itself"
cat <<EOF > .gitassembly
merge master master
EOF

verb "ensure gas fails with an error"
capture not gas -v
assert_out_regex ".gitassembly:1: refusing merge of master into itself"

verb "initialize assembly file with duplicate branch"
cat <<EOF > .gitassembly
merge master branch1 branch2 branch1
EOF

verb "ensure gas fails with an error"
capture not gas -v
assert_out_regex ".gitassembly:1: duplicate merge of branch1 into master"


verb "initialize assembly file with duplicate branch on different commands"
cat <<EOF > .gitassembly
merge master branch1
merge master branch1
EOF

verb "ensure gas fails with an error"
capture not gas -v
assert_out_regex ".gitassembly:2: duplicate merge of branch1 into master"


verb "initialize assembly file with duplicate branch & pattern"
cat <<EOF > .gitassembly
merge master branch1 branch*
EOF

verb "ensure gas fails with a warning"
capture gas -v
assert_out_regex ".gitassembly:1: ignoring duplicate merge of branch1 into master"

verb "initialize assembly file with duplicate branch & pattern on different commands"
cat <<EOF > .gitassembly
merge master branch1
merge master branch*
EOF

verb "ensure gas fails with a warning"
capture gas -v
assert_out_regex ".gitassembly:2: ignoring duplicate merge of branch1 into master"
