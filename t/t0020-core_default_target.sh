#!/bin/sh
DESC="default target handling"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo"
git init -q
touch file
git add file
commit

verb "default to all targets"
cat <<EOF > .gitassembly
merge branch_a master
merge branch_b master
EOF
capture gas
assert_graph_regex 'branch_a\?'
assert_graph_regex 'branch_b\?'

verb "request a single target"
capture gas branch_a
assert_graph_regex 'branch_a\?'
not assert_graph_regex 'branch_b'

verb "request an invalid target"
capture not gas master
assert_out_regex "unknown target branch master"

verb "target through assembly"
cat <<EOF > .gitassembly
merge branch_a master
merge branch_b master
target branch_a
EOF
capture gas
assert_graph_regex 'branch_a\?'
not assert_graph_regex 'branch_b'

verb "implicit target assembly override"
capture gas branch_b
not assert_graph_regex 'branch_a'
assert_graph_regex 'branch_b\?'

verb "explicit target assembly override"
capture gas --all
assert_graph_regex 'branch_a\?'
assert_graph_regex 'branch_b\?'
