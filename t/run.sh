#!/bin/sh
TDIR="$(dirname "$0")"

VERBOSE=0
DESCRIBE=0
HELP=0
PATTERN="t*-*.sh"

while getopts vDh optname
do
  case "$optname" in
  v) VERBOSE=1 ;;
  D) DESCRIBE=1 ;;
  h) HELP=1 ;;
  ?) exit 2 ;;
  esac
done
shift $(($OPTIND - 1))

if [ "$HELP" = 1 ]
then
  echo "Usage: $0 [-Dhv] [pattern]

git-assembler test runner

  -D: describe tests
  -h: this help
  -v: verbose mode"
  exit 0
fi

if [ -n "$1" ]; then
  PATTERN="$1"
fi

ARGS=""
[ "$DESCRIBE" = 1 ] && ARGS="$ARGS -D"
[ "$VERBOSE" = 1 ] && ARGS="$ARGS -v"

# test runner
find "$TDIR" -mindepth 1 -maxdepth 1 -name "$PATTERN" -type f -perm 755 | sort | while read t
do
  printf "%-32s : " "$(basename "$t" .sh)"
  out=$("$t" $ARGS 2>&1)
  v="$?"
  if [ "$v" = 0 ]
  then
    [ "$DESCRIBE" = 1 ] && echo "$out" || echo "ok"
  elif [ "$v" = 96 ]
  then
    [ "$DESCRIBE" = 1 ] && echo "$out" || echo "skip"
  else
    echo "fail"
    if [ "$VERBOSE" = 1 ]
    then
	    echo "========"
	    echo "$out"
    fi
    exit "$v"
  fi
done
