#!/bin/sh
DESC="ensure that git config assembler.mergeff works as expected"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with two branches"
git init -q
touch file_master
git add file_master
commit
checkout -b test

verb "initialize assembly file"
cat <<EOF > .gitassembly
merge master test
EOF

_BASE_HEAD="$(git rev-parse HEAD)"

_reset() {
    checkout master
    git reset --hard -q "$_BASE_HEAD"
    checkout test
    git reset --hard -q master
    touch file_test
    git add file_test
    commit
    _TEST_HEAD="$(git rev-parse HEAD)"
    checkout master
    not test -f file_test
}


assert_out_ff() {
    assert_out_regex "merging test into master using --ff"
    echon "$OUT" grep -q '^git-assembler: running cwd=[^ ]* git merge ' | grep -q ' --ff '
}

assert_out_no_ff() {
    assert_out_regex "merging test into master using --no-ff"
    echon "$OUT" grep -q '^git-assembler: running cwd=[^ ]* git merge ' | grep -q ' --no-ff '
}

assert_out_ff_unset() {
    assert_out_regex "merging test into master"
    echon "$OUT" grep -q '^git-assembler: running cwd=[^ ]* git merge ' | not grep -q ' --\(no-\)\?ff '
}


verb "testing without config"
_reset
capture gas -avvv
assert_out_ff_unset

verb "testing with git config assembler.mergeff true"
_reset
git config assembler.mergeff true
capture gas -avvv
git config --unset assembler.mergeff
assert_out_ff

verb "testing with git config assembler.mergeff false"
_reset
git config assembler.mergeff false
capture gas -avvv
git config --unset assembler.mergeff
assert_out_no_ff

verb "testing with git config GIT_ASSEMBLER_MERGEFF=true"
_reset
GIT_ASSEMBLER_MERGEFF=true capture gas -avvv
assert_out_ff

verb "testing with git config GIT_ASSEMBLER_MERGEFF=true overriding config"
_reset
git config assembler.mergeff false
GIT_ASSEMBLER_MERGEFF=true capture gas -avvv
git config --unset assembler.mergeff
assert_out_ff

verb "testing with git config GIT_ASSEMBLER_MERGEFF=false"
_reset
GIT_ASSEMBLER_MERGEFF=false capture gas -avvv
assert_out_no_ff

verb "testing with git config GIT_ASSEMBLER_MERGEFF=false overriding config"
_reset
git config assembler.mergeff true
GIT_ASSEMBLER_MERGEFF=false capture gas -avvv
git config --unset assembler.mergeff
assert_out_no_ff

verb "testing with git config GIT_ASSEMBLER_MERGEFF= overriding config"
_reset
git config assembler.mergeff true
GIT_ASSEMBLER_MERGEFF= capture gas -avvv
git config --unset assembler.mergeff
assert_out_ff_unset
