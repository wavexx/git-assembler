#!/bin/sh
DESC="test worktree paths for base nodes in graph"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo and two worktree"
git init -q master
(
    cd master
    commit --allow-empty
    git worktree add ../worktree1 -b worktree1 -q
    git worktree add ../worktree2 -b worktree2 -q
)

verb "initialize assembly file"
cat <<EOF > master/.git/assembly
stage test1 master
stage test2 worktree1
EOF

verb "check for relative worktree path in base master"
(
    cd master
    capture gas
    assert_graph_regex '^test1? <= master\*\?$'
    assert_graph_regex '^test2? <= worktree1\*\?(\.\./worktree1)$'
)

verb "check for relative master path in base worktree1"
(
    cd worktree1
    capture gas
    assert_graph_regex '^test1? <= master\*\?(\.\./master)$'
    assert_graph_regex '^test2? <= worktree1\*\?$'
)
