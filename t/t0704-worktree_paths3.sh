#!/bin/sh
DESC="test worktree paths as subtree for merge nodes in graph"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo and a worktree subtree"
git init -q master
(
    cd master
    commit --allow-empty
    git worktree add worktree -b worktree -q
)

verb "initialize assembly file"
cat <<EOF > master/.git/assembly
merge master worktree
EOF

verb "check for relative worktree path in branch master"
(
    cd master
    capture gas
    assert_graph_regex '^  worktree(worktree)$'
)

verb "check for relative master path in branch worktree"
(
    cd master/worktree
    capture gas
    assert_graph_regex '^master(\.\.)$'
)

