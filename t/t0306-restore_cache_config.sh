#!/bin/sh
DESC="cached configuration in partial invocations"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with versioned assembly"
git init -q
echo a > file
cat <<EOF > .gitassembly
merge master temp
EOF
git add file .gitassembly
commit

verb "initialize test branch to force a state save"
checkout -b temp
echo b > file
git add file
commit

verb "break the configuration in the initial branch"
checkout master
echo c > file
cat <<EOF > .gitassembly
error
EOF
git add file .gitassembly
commit

verb "ensure that assembly configuration is invalid"
quiet not gas

verb "run assembler with the valid versioned assembly"
checkout temp
test $(current_branch) = temp
capture not gas -a
assert_out_regex "error while merging temp into master"
test $(current_branch) = master

verb "fix the conflict"
echo b > file
git add file
quiet commit

verb "ensure we can continue with the original valid assembly"
quiet gas -a
test $(current_branch) = temp
