#!/bin/sh
DESC="check that rule flags apply to the correct branches"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo with three branches"
(
    git init -q
    touch file_master
    git add file_master
    commit
    checkout -b test1 master
    touch file_test1
    git add file_test1
    commit
    checkout -b test2 master
    touch file_test2
    git add file_test2
    commit
    checkout master
)

verb "testing rule flag distribution"
(
    cat <<EOF > .gitassembly
    merge master test1 test2 --ff
EOF
    capture gas -av --dry-run
    assert_out_regex "merging test1 into master using --ff"
    assert_out_regex "merging test2 into master using --ff"
)

verb "testing rule flag specificity"
(
    cat <<EOF > .gitassembly
    merge master test1 --ff
    merge master test2 --no-ff
    merge test1 test2 -s ours
EOF
    capture gas -av --dry-run
    assert_out_regex "merging test2 into test1 using -s ours"
    assert_out_regex "merging test1 into master using --ff"
    assert_out_regex "merging test2 into master using --no-ff"
)
