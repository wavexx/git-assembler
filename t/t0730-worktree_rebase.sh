#!/bin/sh
DESC="basic rebase tests on worktrees"
. "$(dirname "$0")/lib.sh"

verb "initialize a new repo and worktree"
git init -q master
(
    cd master
    touch file_master
    git add file_master
    commit
    git worktree add ../worktree -b test -q
    touch file_new
    git add file_new
    commit
)

verb "update the worktree"
(
    cd worktree
    touch file_test
    git add file_test
    commit
)

verb "initialize assembly file"
cd master
cat <<EOF > .gitassembly
rebase test master
EOF

verb "ensure rebase is run in the worktree"
capture gas -av
assert_out_regex 'rebasing test(\.\./worktree) onto master'
test -f ../worktree/file_new
