git-assembler 1.5: 2023-06-12
-----------------------------

* ``merge`` and ``rebase`` commands now accept git flags directly,
  allowing to choose a different merge strategy or customizing git
  behavior on a branch-by-branch basis.
* A new command ``flags`` has been introduced to setup default flags for
  merge or rebase operations. Default and per-branch flags are combined,
  with the latter taking precedence.
* The default value for the ``assembly.mergeff`` setting has been
  changed to "unset" in order to match git defaults.
* When resuming an interrupted operation, `git-assembler` now refuses to
  read a state file produced by an earlier incompatible version instead
  of failing.


git-assembler 1.4: 2023-05-18
-----------------------------

Major changes:

* Worktrees are now fully supported:

  - Checked-out branches are correctly updated/rebuilt from any tree
    when referenced in the current assembly
  - Worktree paths are now shown next to branch names when checked-out
  - Interrupted operations that span more than one tree correctly
    restore the initial state upon completion

* The current assembly file is now cached when interrupted, allowing a
  pending operation to complete correctly without being overridden by
  local tree changes.
* The ``--clear`` flag has been added to forget about any pending state.
* `git-assembler` now requires git v2.23.0 or greater.

Bug fixes:

* Fix ``merge`` rules containing multiple merge branches (regression in
  1.3, thanks to Etienne Samson)
* Fix editor invocation under Windows when the assembly path contains
  special characters (thanks to Etienne Samson)
* Reworked branch prefixes and sigils in the graph output in order to
  show used and dirty states also when output coloring is disabled
  (thanks to Etienne Samson)

Notably the '<' branch prefix is now used in the output graph instead of
'*' to avoid confusion with a branch with updated content. See the
documentation for more details.


git-assembler 1.3: 2022-12-05
-----------------------------

* Rules now support wildcard patterns (glob-like or regex) in both the
  target of a rule or as any dependency.
* `git-assembler` now ignores versioned ``.gitassembler`` files that are
  symlinked to avoid reading from a remotely-controlled file location.
* A new ``-e``/``--edit`` flag has been added to automatically spawn an
  editor on the currently active assembly file.
* `git-assembler` now passes-through branch/ref names as binary, without
  assuming any specific encoding, matching git's underlying behavior.
* The default merge fast-forward behavior (``--ff`` or ``--no-ff``) can
  now be controlled using the ``assembler.mergeff`` configuration entry.
  Thanks to Erich Gubler and Gustavo Sousa.
* Documentation fixes thanks to Kipras Melnikovas.


git-assembler 1.2: 2020-09-23
-----------------------------

Improved worktree support thanks to Etienne Laurin:

* Allow to create staging branches even when the base branch is already
  checked out in a different worktree.
* Fix internal state paths when using worktrees.

General bug fixes:

* Fix dirty graph state/coloring when using ``base`` and merging with
  the base branch itself.
* Read correctly assembly files with a missing EOL (thanks to
  Etienne Laurin).
* Handle checkout errors without a spurious traceback when referring to
  undefined branches.

Minor improvements:

* Remove useless commit error messages when performing a rerere
  autocommit.


git-assembler 1.1: 2020-08-03
-----------------------------

* Fixes ``rebase`` behavior, thanks to Richard Nguyen.
* Tests portability fixes, thanks to Carlo Arenas.
* New ``--color`` flag to control terminal coloring.
* Fixes failure to restore starting branch in some cases.
